package ca.promutuel.csd.inventaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.promutuel.csd.inventaire.model.EnvironnementApplication;

public interface EnvironnementApplicationRepository extends JpaRepository<EnvironnementApplication, Long> {

}
