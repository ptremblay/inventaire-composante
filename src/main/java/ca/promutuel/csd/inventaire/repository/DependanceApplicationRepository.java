package ca.promutuel.csd.inventaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.promutuel.csd.inventaire.model.DependanceApplication;

public interface DependanceApplicationRepository extends JpaRepository<DependanceApplication, Long>  {

}
