package ca.promutuel.csd.inventaire.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.promutuel.csd.inventaire.model.Application;

public interface ApplicationRepository extends JpaRepository<Application, Long> {
	
	public List<Application> findByNom(String nom);
	
	public List<Application> findByCode(String code);

}
