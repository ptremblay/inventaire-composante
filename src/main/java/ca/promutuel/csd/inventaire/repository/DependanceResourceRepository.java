package ca.promutuel.csd.inventaire.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.promutuel.csd.inventaire.model.DependanceResource;

public interface DependanceResourceRepository extends JpaRepository<DependanceResource, Long> {

}
