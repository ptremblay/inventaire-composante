package ca.promutuel.csd.inventaire.config;

import javax.sql.DataSource;

public interface PersistenceConfig {
	
	public DataSource dataSource();

}
