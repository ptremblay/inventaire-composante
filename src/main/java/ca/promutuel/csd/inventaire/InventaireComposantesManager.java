package ca.promutuel.csd.inventaire;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import ca.promutuel.csd.inventaire.config.InventaireComposantesConfig;
import ca.promutuel.csd.inventaire.service.InventaireComposanteService;

@Configuration
@PropertySource("classpath:application.properties")
public class InventaireComposantesManager {
	
    private static final String SPRING_PROFILES_ACTIVE = "devlocal";
	private static final String BASE_PACKAGE_TO_SCAN = "ca.promutuel.csd.inventaire";

	public static void main(String[] args) {
    	
    	AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
    	context.getEnvironment().setActiveProfiles(SPRING_PROFILES_ACTIVE);
    	context.register(InventaireComposantesManager.class);
    	context.register(InventaireComposantesConfig.class);
    	context.scan(BASE_PACKAGE_TO_SCAN);
    	context.refresh();
    	
        InventaireComposanteService service = context.getBean(InventaireComposanteService.class);
        service.refreshDatabase();
        
        context.close();
    }

}