package ca.promutuel.csd.inventaire.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.promutuel.csd.inventaire.model.Application;
import ca.promutuel.csd.inventaire.model.DependanceApplication;
import ca.promutuel.csd.inventaire.model.DependanceResource;
import ca.promutuel.csd.inventaire.model.EnvironnementApplication;
import ca.promutuel.csd.inventaire.reader.ApplicationResource;
import ca.promutuel.csd.inventaire.reader.EtatSante;
import ca.promutuel.csd.inventaire.reader.EtatSanteReader;
import ca.promutuel.csd.inventaire.reader.Resource;
import ca.promutuel.csd.inventaire.reader.SaltstackApplication;
import ca.promutuel.csd.inventaire.reader.SaltstackApplicationReader;
import ca.promutuel.csd.inventaire.reader.SaltstackDeployment;
import ca.promutuel.csd.inventaire.reader.SaltstackEnvironment;
import ca.promutuel.csd.inventaire.reader.SaltstackEnvironmentReader;

@Service
public class InventaireComposanteServiceImpl implements
		InventaireComposanteService {

	@Autowired
	ApplicationService applicationService;
	
	@Autowired
	EnvironnementApplicationService environnementApplicationService;

	@Autowired
	DependanceResourceService dependanceResourceService;
	
	@Autowired
	DependanceApplicationService dependanceApplicationService;

	@Override
	public void refreshDatabase() {
		
		List<SaltstackApplication> saltstackApplication = SaltstackApplicationReader.newInstance().getApplications();
		List<SaltstackEnvironment> saltstackEnvironment = SaltstackEnvironmentReader.newInstance().getEnvironnements();
		
		/* 
		 * Cette map servira au chargement de la table 
		 * DEP_APPLICATION et sera chargé en même temps que la table APPLICATION
		 * 
		 * Elle a un faible volume et évitera des round trip inutile a la bd
		 * 
		 */
		Map<String,String> applicationMap = new HashMap<String,String>();
		

		
		//Chargement de la table APPLICATIONS
		for (SaltstackApplication sa : saltstackApplication) {

			Application a = new Application();
			a.setCode((sa.getNom().toUpperCase().replaceAll("[^a-zA-Z]+","")));
			a.setNom(sa.getNom());
			a.setKeywordUrl(sa.getContexte());
			applicationService.save(a);
			
			applicationMap.put(a.getCode(), a.getKeywordUrl());

		}
		
		//Chargement des tables DEP_RESOURCE et ENV_APPLICATION
		String environnement = null;
		Application application = null;
		
		for (SaltstackEnvironment se : saltstackEnvironment){
			environnement = se.getEnvName();
			
			for (SaltstackDeployment deployment : se.getDeploiements()){
				
				List<Application> applications = applicationService.findByNom(deployment.getApplicationName());
				
				if (applications != null && !applications.isEmpty()) {
					application = applications.get(0);
					
					for (String server : deployment.getServers()){
						
						EtatSante etatSante = EtatSanteReader.newInstanceFromServerName(server, application.getKeywordUrl(),applicationMap).getEtatSante();
						
						EnvironnementApplication environnementApplication = new EnvironnementApplication();
						
						environnementApplication.setApplicationCode(application.getCode());
						environnementApplication.setEnvironnement(environnement);
						environnementApplication.setServeur(server);
						environnementApplication.setApplicationVersion(etatSante.getVersion());
						environnementApplication.setUrlEtatSante(etatSante.getUrl());
						
						if (etatSante.isRequestTimedOut()) {
							environnementApplication.setHttpResponseEtatSante("Timeout. Pas de réponse");
						} else {
							environnementApplication.setHttpResponseEtatSante(etatSante
									.getHttpResponseCode()
									+ " - "
									+ etatSante.getHttpResponseMessage());
						}
						
					    environnementApplicationService.save(environnementApplication);
						
						for (Resource resource : etatSante.getResources()){
							
							DependanceResource dependanceResource = new DependanceResource();
							
							dependanceResource.setApplicationCode(application.getCode());
							dependanceResource.setEnvironnement(environnement);
							dependanceResource.setServeur(server);
							dependanceResource.setResourceType(resource.getType().toString());
							dependanceResource.setResourceValeur(resource.getValue());
							dependanceResource.setResourceServeur(resource.getServeur());
							dependanceResource.setResourceVersion(resource.getAppVersion());
							dependanceResource.setResourcePort(resource.getPort());
							dependanceResource.setDatabaseType(resource.getDatabaseType());
							dependanceResource.setDatabaseName(resource.getDatabaseName());
							dependanceResource.setMsgqDestination(resource.getMsgqDestination());
							dependanceResource.setMsgqIdentifiant(resource.getMsgqIdentifiant());
							
							dependanceResourceService.save(dependanceResource);
							
							if (resource.getType().toString().equals("APPS")) {

								DependanceApplication dependanceApplication = new DependanceApplication();
								dependanceApplication.setApplicationCode(application.getCode());
								dependanceApplication.setEnvironnement(environnement);
								dependanceApplication.setServeur(server);
								dependanceApplication.setDependanceApplicationCode(((ApplicationResource) resource).getApplicationCode());
								dependanceApplication.setDependanceEnvironnement(((ApplicationResource) resource).getApplicationEnvironnement());
								dependanceApplication.setDependanceApplicationVersion(((ApplicationResource) resource).getApplicationVersion());
								dependanceApplicationService.save(dependanceApplication);
							}
						}
					}
				}
			}
		}
	}
}
