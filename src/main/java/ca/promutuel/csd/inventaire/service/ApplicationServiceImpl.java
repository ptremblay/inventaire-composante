package ca.promutuel.csd.inventaire.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import ca.promutuel.csd.inventaire.exception.ApplicationNotFoundException;
import ca.promutuel.csd.inventaire.model.Application;
import ca.promutuel.csd.inventaire.repository.ApplicationRepository;


@Service
public class ApplicationServiceImpl implements ApplicationService {

	@Resource
	private ApplicationRepository applicationRepository;

	@Override
	public Application delete(Long applicationId) throws ApplicationNotFoundException {
		return null;
	}

	@Override
	public List<Application> findAll() {
		return applicationRepository.findAll();
	}

	@Override
	public Application findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Application> findByCode(String code) {
		return applicationRepository.findByCode(code);
	}

	@Override
	public Application save(Application application) {
		return applicationRepository.save(application);
	}

	@Override
	public List<Application> findByNom(String nom) {
		return applicationRepository.findByNom(nom);
	}

}
