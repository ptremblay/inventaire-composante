package ca.promutuel.csd.inventaire.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import ca.promutuel.csd.inventaire.exception.EnvironnementApplicationNotFoundException;
import ca.promutuel.csd.inventaire.model.EnvironnementApplication;
import ca.promutuel.csd.inventaire.repository.EnvironnementApplicationRepository;

@Service
public class EnvironnementApplicationServiceImpl implements
		EnvironnementApplicationService {

	@Resource
	private EnvironnementApplicationRepository environnementApplicationRepository;
	
	@Override
	public EnvironnementApplication delete(Long environnementApplicationId)
			throws EnvironnementApplicationNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EnvironnementApplication> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EnvironnementApplication findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EnvironnementApplication save(
			EnvironnementApplication environnementApplication) {
		return environnementApplicationRepository.save(environnementApplication);
	}

}
