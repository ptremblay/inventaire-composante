package ca.promutuel.csd.inventaire.service;

import java.util.List;

import ca.promutuel.csd.inventaire.exception.EnvironnementApplicationNotFoundException;
import ca.promutuel.csd.inventaire.model.EnvironnementApplication;

public interface EnvironnementApplicationService {
	
	public EnvironnementApplication delete(Long environnementApplicationId) throws EnvironnementApplicationNotFoundException;

	public List<EnvironnementApplication> findAll();

	public EnvironnementApplication findById(Long id);
	
	public EnvironnementApplication save(EnvironnementApplication environnementApplication);

}
