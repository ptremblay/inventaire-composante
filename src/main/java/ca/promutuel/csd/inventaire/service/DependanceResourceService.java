package ca.promutuel.csd.inventaire.service;

import java.util.List;

import ca.promutuel.csd.inventaire.model.DependanceResource;

public interface DependanceResourceService {

	public List<DependanceResource> findAll();

	public DependanceResource save(DependanceResource dependanceResource);

}
