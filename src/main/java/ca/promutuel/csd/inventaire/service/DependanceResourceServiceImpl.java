package ca.promutuel.csd.inventaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.promutuel.csd.inventaire.model.DependanceResource;
import ca.promutuel.csd.inventaire.repository.DependanceResourceRepository;

@Service
public class DependanceResourceServiceImpl implements DependanceResourceService {

	@Autowired
	private DependanceResourceRepository dependanceResourceRepository;

	@Override
	public List<DependanceResource> findAll() {
		return dependanceResourceRepository.findAll();
	}

	@Override
	public DependanceResource save(DependanceResource dependanceResource) {
		return dependanceResourceRepository.save(dependanceResource);
	}

}
