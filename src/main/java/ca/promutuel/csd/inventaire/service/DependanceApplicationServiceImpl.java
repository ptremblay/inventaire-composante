package ca.promutuel.csd.inventaire.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.promutuel.csd.inventaire.model.DependanceApplication;
import ca.promutuel.csd.inventaire.repository.DependanceApplicationRepository;

@Service
public class DependanceApplicationServiceImpl implements
		DependanceApplicationService {

	@Autowired
	DependanceApplicationRepository dependanceApplicationRepository;
	
	
	@Override
	public List<DependanceApplication> findAll() {
		return dependanceApplicationRepository.findAll();
	}

	@Override
	public DependanceApplication save(
			DependanceApplication dependanceApplication) {
		return dependanceApplicationRepository.save(dependanceApplication);
	}

}
