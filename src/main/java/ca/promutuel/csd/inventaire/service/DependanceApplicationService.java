package ca.promutuel.csd.inventaire.service;

import java.util.List;

import ca.promutuel.csd.inventaire.model.DependanceApplication;

public interface DependanceApplicationService {

	public List<DependanceApplication> findAll();

	public DependanceApplication save(DependanceApplication dependanceApplication);

}
