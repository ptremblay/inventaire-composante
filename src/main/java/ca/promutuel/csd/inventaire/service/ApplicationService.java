package ca.promutuel.csd.inventaire.service;

import java.util.List;

import ca.promutuel.csd.inventaire.exception.ApplicationNotFoundException;
import ca.promutuel.csd.inventaire.model.Application;



public interface ApplicationService {

	public Application delete(Long applicationId) throws ApplicationNotFoundException;

	public List<Application> findAll();

	public Application findById(Long id);
	
	public List<Application> findByCode(String code);
	
	public List<Application> findByNom(String nom);
	
	public Application save(Application application);

}
