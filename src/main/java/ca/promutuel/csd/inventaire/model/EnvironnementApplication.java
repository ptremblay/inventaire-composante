package ca.promutuel.csd.inventaire.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "ENV_APPLICATION")
public class EnvironnementApplication {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ENVIRONNEMENT", nullable = false)
	private String environnement;
	
	@Column(name = "SERVEUR", nullable = false)
	private String serveur;
	
	@Column(name = "APPLICATION_CODE", nullable = false)
	private String applicationCode;

	@Column(name = "APPLICATION_VERSION", nullable = true)
	private String applicationVersion;
	
	@Column(name = "URL_ETAT_SANTE", nullable = true)
	private String urlEtatSante;
	
	@Column(name = "HTTP_RESPONSE_ETAT_SANTE", nullable = true)
	private String httpResponseEtatSante;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnvironnement() {
		return environnement;
	}

	public void setEnvironnement(String environnement) {
		this.environnement = environnement;
	}

	public String getServeur() {
		return serveur;
	}

	public void setServeur(String serveur) {
		this.serveur = serveur;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}
	
	public String getUrlEtatSante() {
		return urlEtatSante;
	}

	public void setUrlEtatSante(String urlEtatSante) {
		this.urlEtatSante = urlEtatSante;
	}
	
	public String getHttpResponseEtatSante() {
		return httpResponseEtatSante;
	}

	public void setHttpResponseEtatSante(String httpResponseEtatSante) {
		this.httpResponseEtatSante = httpResponseEtatSante;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
