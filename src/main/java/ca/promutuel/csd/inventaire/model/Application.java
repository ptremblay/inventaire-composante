package ca.promutuel.csd.inventaire.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "APPLICATION")
public class Application {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "CODE", nullable = false)
	private String code;

	@Column(name = "NOM", nullable = false)
	private String nom;

	@Column(name = "KEYWORD_URL", nullable = false)
	private String keywordUrl;

	public Application() {
	};

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getKeywordUrl() {
		return keywordUrl;
	}

	public void setKeywordUrl(String keywordUrl) {
		this.keywordUrl = keywordUrl;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
