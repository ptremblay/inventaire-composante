package ca.promutuel.csd.inventaire.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DEP_RESOURCE")
public class DependanceResource {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ENVIRONNEMENT", nullable = false)
	private String environnement;

	@Column(name = "SERVEUR", nullable = false)
	private String serveur;

	@Column(name = "APPLICATION_CODE", nullable = false)
	private String applicationCode;

	@Column(name = "RESOURCE_TYPE", nullable = true)
	private String resourceType;

	@Column(name = "RESOURCE_VALEUR", nullable = true)
	private String resourceValeur;

	@Column(name = "RESOURCE_SERVEUR", nullable = true)
	private String resourceServeur;

	@Column(name = "RESOURCE_PORT", nullable = true)
	private String resourcePort;
	
	@Column(name = "DATABASE_TYPE", nullable = true)
	private String databaseType;
	
	@Column(name = "DATABASE_NAME", nullable = true)
	private String databaseName;
	
	@Column(name = "RESOURCE_VERSION", nullable = true)
	private String resourceVersion;
	
	@Column(name = "MSGQ_DESTINATION", nullable = true)
	private String msgqDestination;
	
	@Column(name = "MSGQ_IDENTIFIANT", nullable = true)
	private String msgqIdentifiant;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnvironnement() {
		return environnement;
	}

	public void setEnvironnement(String environnement) {
		this.environnement = environnement;
	}

	public String getServeur() {
		return serveur;
	}

	public void setServeur(String serveur) {
		this.serveur = serveur;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceValeur() {
		return resourceValeur;
	}

	public void setResourceValeur(String resourceValeur) {
		this.resourceValeur = resourceValeur;
	}

	public String getResourceServeur() {
		return resourceServeur;
	}

	public void setResourceServeur(String resourceServeur) {
		this.resourceServeur = resourceServeur;
	}

	public String getResourcePort() {
		return resourcePort;
	}

	public void setResourcePort(String resourcePort) {
		this.resourcePort = resourcePort;
	}

	public String getDatabaseType() {
		return databaseType;
	}

	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getMsgqDestination() {
		return msgqDestination;
	}

	public void setMsgqDestination(String msgqDestination) {
		this.msgqDestination = msgqDestination;
	}

	public String getMsgqIdentifiant() {
		return msgqIdentifiant;
	}

	public void setMsgqIdentifiant(String msgqIdentifiant) {
		this.msgqIdentifiant = msgqIdentifiant;
	}

	public String getResourceVersion() {
		return resourceVersion;
	}

	public void setResourceVersion(String resourceVersion) {
		this.resourceVersion = resourceVersion;
	}
	
}
