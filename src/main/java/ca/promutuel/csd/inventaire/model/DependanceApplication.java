package ca.promutuel.csd.inventaire.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DEP_APPLICATION")
public class DependanceApplication {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ENVIRONNEMENT", nullable = false)
	private String environnement;

	@Column(name = "SERVEUR", nullable = false)
	private String serveur;

	@Column(name = "APPLICATION_CODE", nullable = false)
	private String applicationCode;

	@Column(name = "DEP_ENVIRONNEMENT", nullable = false)
	private String dependanceEnvironnement;

	@Column(name = "DEP_APPLICATION_CODE", nullable = false)
	private String dependanceApplicationCode;
	
	@Column(name = "DEP_APPLICATION_VERSION", nullable = true)
	private String dependanceApplicationVersion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnvironnement() {
		return environnement;
	}

	public void setEnvironnement(String environnement) {
		this.environnement = environnement;
	}

	public String getServeur() {
		return serveur;
	}

	public void setServeur(String serveur) {
		this.serveur = serveur;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getDependanceEnvironnement() {
		return dependanceEnvironnement;
	}

	public void setDependanceEnvironnement(String dependanceEnvironnement) {
		this.dependanceEnvironnement = dependanceEnvironnement;
	}

	public String getDependanceApplicationCode() {
		return dependanceApplicationCode;
	}

	public void setDependanceApplicationCode(String dependanceApplicationCode) {
		this.dependanceApplicationCode = dependanceApplicationCode;
	}

	public String getDependanceApplicationVersion() {
		return dependanceApplicationVersion;
	}

	public void setDependanceApplicationVersion(String dependanceApplicationVersion) {
		this.dependanceApplicationVersion = dependanceApplicationVersion;
	}
	
}
