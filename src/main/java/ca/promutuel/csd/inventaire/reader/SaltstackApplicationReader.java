package ca.promutuel.csd.inventaire.reader;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.codec.binary.Base64;

public class SaltstackApplicationReader {

    private static final String APPS_CSV = "http://envs.dev.promutuel.local/securise/apps.csv";
	//private static final String APPS_CSV = "file:///T:/data/apps.csv";
	private static final String HEADER = "nom;contexte;diminutif;port_app";

	private SaltstackApplicationReader() {
	}

	public static SaltstackApplicationReader newInstance() {
		SaltstackApplicationReader sar = new SaltstackApplicationReader();
		sar.readAppsCsv();
		return sar;
	}

	private List<SaltstackApplication> applications = new ArrayList<SaltstackApplication>();

	public List<SaltstackApplication> getApplications() {
		return applications;
	}

	private void readAppsCsv() {

		applications = new ArrayList<SaltstackApplication>();

		try {
			String webPage = APPS_CSV;
			String name = Credentials.getUsername();
			String password = Credentials.getPassword();

			String authString = name + ":" + password;
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			URL url = new URL(webPage);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic "
					+ authStringEnc);
			InputStream is = urlConnection.getInputStream();

			DataInputStream dis = new DataInputStream(is);
			BufferedReader br = new BufferedReader(new InputStreamReader(dis));

			String line;
			while ((line = br.readLine()) != null) {
				if (line.length() != 0 && !line.equals(HEADER)) {
					applications.add(buildApplicationFromLine(line));
				}
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	};

	private SaltstackApplication buildApplicationFromLine(String line) {

		SaltstackApplication app = new SaltstackApplication();

		StringTokenizer lineTokenizer = new StringTokenizer(line, ";");
		int lineTokenizerInd = 0;

		while (lineTokenizer.hasMoreTokens()) {
			String lineToken = lineTokenizer.nextToken();

			if (lineTokenizerInd == 0) {
				app.setNom(lineToken);
			} else if (lineTokenizerInd == 1) {
				app.setContexte(lineToken);
			} else if (lineTokenizerInd == 2) {
				app.setDiminutif(lineToken);
			} else if (lineTokenizerInd == 3) {
				app.setPort(lineToken);
			}

			lineTokenizerInd++;
		}

		return app;

	}

}
