package ca.promutuel.csd.inventaire.reader;

public class ApplicationResource extends Resource {

	private String applicationVersion;
	private String applicationCode;
	private String applicationEnvironnement;

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getApplicationEnvironnement() {
		return applicationEnvironnement;
	}

	public void setApplicationEnvironnement(String applicationEnvironnement) {
		this.applicationEnvironnement = applicationEnvironnement;
	}
	
}
