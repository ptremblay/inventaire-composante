package ca.promutuel.csd.inventaire.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class SaltstackEnvironmentReader {

	private static final String SALT_ENVS_JSON_DATASOURCE = "http://envs/envi.json";
	private List<SaltstackEnvironment> environnements = new ArrayList<SaltstackEnvironment>();

	private SaltstackEnvironmentReader() {
	}

	public static SaltstackEnvironmentReader newInstance() {
		SaltstackEnvironmentReader ser = new SaltstackEnvironmentReader();
		ser.readEnvsJson();
		return ser;
	}

	public List<SaltstackEnvironment> getEnvironnements() {
		return environnements;
	}

	
	private void readEnvsJson() {

		try {
			String webPage = SALT_ENVS_JSON_DATASOURCE;
			String name = Credentials.getUsername();
			String password = Credentials.getPassword();

			String authString = name + ":" + password;
			byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
			String authStringEnc = new String(authEncBytes);

			URL url = new URL(webPage);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic "
					+ authStringEnc);
			InputStream is = urlConnection.getInputStream();

			InputStreamReader reader = new InputStreamReader(is);

			GsonBuilder builder = new GsonBuilder();
			Gson gson = builder.create();

			JsonObject envs = gson.fromJson(reader, JsonObject.class);
			
			for (Entry<String, JsonElement> envsEntry : envs.entrySet()) {
				
				SaltstackEnvironment sse = new SaltstackEnvironment();
				sse.setEnvName(envsEntry.getKey());
				JsonObject env = gson.fromJson(envsEntry.getValue(), JsonObject.class);
				
				for (Entry<String, JsonElement> envEntry : env.entrySet()) {
					
					String key = envEntry.getKey();
					
					if(key.equals("projet")){
						sse.setProjet(envEntry.getValue().getAsString());
					}else if (key.equals("port_env")){
						sse.setPort_env(envEntry.getValue().getAsString());
					}else if (key.equals("netscaler")){
						sse.setNetscaler(envEntry.getValue().getAsString());
					}else if (key.equals("equipe")){
						sse.setEquipe(envEntry.getValue().getAsString());
					}else if (key.equals("livraison")){
						sse.setLivraison(envEntry.getValue().getAsString());
					}else if (key.equals("distribution")){
						
						JsonArray distArray = gson.fromJson(envEntry.getValue(), JsonArray.class);
						
						for(JsonElement jeDist : distArray ){
							
							JsonObject dist = jeDist.getAsJsonObject();
	
							for (Entry<String, JsonElement> distEntry : dist.entrySet()) {
								SaltstackDeployment deployment = new SaltstackDeployment();
								deployment.setApplicationName(distEntry.getKey());
								
								JsonArray serverArray = gson.fromJson(distEntry.getValue(), JsonArray.class);
								for(JsonElement jeServer : serverArray ){
									deployment.getServers().add(jeServer.getAsString());
								}
								
								sse.getDeploiements().add(deployment);
								
							}
							
						}
						
					}else if (key.equals("palier")){
						sse.setPalier(envEntry.getValue().getAsString());
					}
					
				}
				
				environnements.add(sse);
				
			}


		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
