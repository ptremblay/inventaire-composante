package ca.promutuel.csd.inventaire.reader.parsers;

public interface ResourceValueParser {
		
	public void parse(String resourceValue);

}
