package ca.promutuel.csd.inventaire.reader;

public enum DatabaseType {
	
	DB2, SQLSERVER, H2;

}
