package ca.promutuel.csd.inventaire.reader.parsers;

import java.util.StringTokenizer;

public class MSGQResourceValueParser implements ResourceValueParser {
	
	private String msgqDestination;
	private String msgqIdentifiant;

	@Override
	public void parse(String resourceValue) {
		
		if (resourceValue.contains("Destination=") && resourceValue.contains("Identifiant=")) {

			StringTokenizer tokenizer = new StringTokenizer(resourceValue,",");

			int ind = 0;
			
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				if (ind == 0) {
					StringTokenizer tokenizer2 = new StringTokenizer(token,"=");
					int ind2 = 0;
					while (tokenizer2.hasMoreTokens()) {
						String token2 = tokenizer2.nextToken();
						if (ind2 == 1) {
							msgqDestination = token2;
						}
						ind2++;
					}
				}
				if (ind == 1) {
					StringTokenizer tokenizer3 = new StringTokenizer(token,"=");
					int ind3 = 0;
					while (tokenizer3.hasMoreTokens()) {
						String token3 = tokenizer3.nextToken();
						if (ind3 == 1) {
							msgqIdentifiant = token3;
						}
						ind3++;
					}
				}
				ind++;
			}
		}
	}
		

	public String getMsgqDestination() {
		return msgqDestination;
	}

	public String getMsgqIdentifiant() {
		return msgqIdentifiant;
	}


}
