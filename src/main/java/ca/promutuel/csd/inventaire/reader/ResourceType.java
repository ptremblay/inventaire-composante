package ca.promutuel.csd.inventaire.reader;

public enum ResourceType {

	DB, LDAP, HTTP, SMTP, MSGQ, FTP, RLVL, APPS

}
