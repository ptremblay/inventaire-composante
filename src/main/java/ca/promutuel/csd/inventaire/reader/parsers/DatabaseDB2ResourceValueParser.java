package ca.promutuel.csd.inventaire.reader.parsers;

import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

public class DatabaseDB2ResourceValueParser implements ResourceValueParser {
	
	private String server;
	private String port;
	private String databaseName;

	@Override
	public void parse(String resourceValue) {
		
		if (resourceValue.startsWith("jdbc:db2://")) {

			String prepared = StringUtils.replace(StringUtils.removeStart(resourceValue,"jdbc:db2://"), "/", ":");
			StringTokenizer tokenizer = new StringTokenizer(prepared,":");

			int ind = 0;
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				if (ind == 0) {
					server = token;
				}
				if (ind == 1) {
					port = token;
				}
				if (ind == 2) {
					databaseName = token;
				}
				ind++;
			}
		}

	}


	public String getServer() {
		return server;
	}

	public String getPort() {
		return port;
	}

	public String getName() {
		return databaseName;
	}

}
