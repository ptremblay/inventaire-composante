package ca.promutuel.csd.inventaire.reader.parsers;

import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

public class FTPResourceValueParser implements ResourceValueParser {
	
	private String server;
	private String port;

	@Override
	public void parse(String resourceValue) {
		
		if (resourceValue.startsWith("ftp://")) {

			String prepared = StringUtils.removeStart(resourceValue,"ftp://");
			StringTokenizer tokenizer = new StringTokenizer(prepared,":");

			int ind = 0;
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				if (ind == 0) {
					server = token;
				}
				if (ind == 1) {
					port = token;
				}
				ind++;
			}
		}
	}

	public String getServer() {
		return server;
	}

	public String getPort() {
		return port;
	}

}
