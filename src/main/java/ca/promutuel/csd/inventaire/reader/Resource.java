package ca.promutuel.csd.inventaire.reader;

public class Resource {

	private ResourceType type;
	private String value;
	private String serveur;
	private String port;
	private String databaseType;
	private String databaseName;
	private String msgqDestination;
	private String msgqIdentifiant;
	private String appVersion;

	public ResourceType getType() {
		return type;
	}

	public void setType(ResourceType type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getServeur() {
		return serveur;
	}

	public void setServeur(String serveur) {
		this.serveur = serveur;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDatabaseType() {
		return databaseType;
	}

	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getMsgqDestination() {
		return msgqDestination;
	}

	public void setMsgqDestination(String msgqDestination) {
		this.msgqDestination = msgqDestination;
	}

	public String getMsgqIdentifiant() {
		return msgqIdentifiant;
	}

	public void setMsgqIdentifiant(String msgqIdentifiant) {
		this.msgqIdentifiant = msgqIdentifiant;
	}
	
}
