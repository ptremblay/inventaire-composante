package ca.promutuel.csd.inventaire.reader;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.springframework.util.StringUtils;

import ca.promutuel.csd.inventaire.reader.parsers.DatabaseDB2ResourceValueParser;
import ca.promutuel.csd.inventaire.reader.parsers.DatabaseSQLServerResourceValueParser;
import ca.promutuel.csd.inventaire.reader.parsers.FTPResourceValueParser;
import ca.promutuel.csd.inventaire.reader.parsers.HTTPResourceValueParser;
import ca.promutuel.csd.inventaire.reader.parsers.LDAPResourceValueParser;
import ca.promutuel.csd.inventaire.reader.parsers.MSGQResourceValueParser;
import ca.promutuel.csd.inventaire.reader.parsers.SMTPResourceValueParser;

public class EtatSanteReader {

	private EtatSante etatSante = new EtatSante();
	
	private Map<String,String> applicationMap;

	private EtatSanteReader() {
	}

	public static EtatSanteReader newInstanceFromServerName(String serveur,
			String contextRoot, Map<String,String> applicationMap) {
		EtatSanteReader esr = new EtatSanteReader();
		esr.getEtatSante().setUrl(
				"http://" + serveur + "/" + contextRoot + "/etat-sante?detail=true");
		esr.applicationMap = applicationMap;
		esr.readEtatSante();
		return esr;
	}

	private void readEtatSante() {

		try {
			String webPage = getEtatSante().getUrl();

			URL url = new URL(webPage);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setReadTimeout(5000);
			urlConnection.connect();

			if (urlConnection instanceof HttpURLConnection) {
				HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;
				etatSante.setHttpResponseCode((String.valueOf(httpConnection
						.getResponseCode())));
				etatSante.setHttpResponseMessage(httpConnection
						.getResponseMessage());
			}

			InputStream is = urlConnection.getInputStream();

			DataInputStream dis = new DataInputStream(is);
			BufferedReader br = new BufferedReader(new InputStreamReader(dis));

			String line;
			StringBuffer content = new StringBuffer();

			while ((line = br.readLine()) != null) {
				content.append(line);

				// Extraire version
				if (line.startsWith("Version")) {
					etatSante.setVersion(extractVersion(line));
				}

				// Extraire dependance BD
				if (line.contains("BD") && (line.contains("jdbc:"))) {
					etatSante.getResources().add(extractDatabaseResource(line));
				}

				// Extraire dependance LDAP
				if (line.contains("LDAP")) {
					etatSante.getResources().add(extractLDAPResource(line));
				}

				// Extraire dependance HTTP
				if (line.contains("HTTP") && (line.contains("http://"))) {
					Resource httpResource = extractHTTPResource(line);
					ApplicationResource applicationResource = toApplicationResource(httpResource);
					if (applicationResource.getApplicationCode() != null) {
						etatSante.getResources().add(extractAPPSResource(line, applicationResource));
					}else {
						etatSante.getResources().add(extractHTTPResource(line));
					}
				}
				
				// Extraire dependance FTP
				if (line.contains("FTP") && (line.contains("ftp://"))) {
					etatSante.getResources().add(extractFTPResource(line));
				
				}
				
				// Extraire dependance SMTP
				if (line.contains("SMTP") && (line.contains("smtp://"))) {
					etatSante.getResources().add(extractSMTPResource(line));
				
				}
				
				// Extraire dependance MSGQ 
				if (line.contains("MSGQ") && (line.contains("Destination="))) {
					etatSante.getResources().add(extractMSGQResource(line));
				
				}

			}
			this.etatSante.setContent(content.toString());

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			etatSante.setRequestTimedOut(true);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private Resource extractMSGQResource(String line) {
		
		Resource resource = new Resource();
		resource.setType(ResourceType.MSGQ);

		StringTokenizer st = new StringTokenizer(line, "|");
		int ind = 0;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (ind == 2) {
				resource.setValue(StringUtils.trimWhitespace(token));
			}
			ind++;
		}

		MSGQResourceValueParser msgqParser = new MSGQResourceValueParser();
		msgqParser.parse(resource.getValue());
		resource.setMsgqDestination(msgqParser.getMsgqDestination());
		resource.setMsgqIdentifiant(msgqParser.getMsgqIdentifiant());

		return resource;
	}

	private Resource extractAPPSResource(String line, ApplicationResource applicationResource) {
		
		applicationResource.setType(ResourceType.APPS);
		
		StringTokenizer st = new StringTokenizer(line, "|");
		int ind = 0;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (ind == 2) {
				applicationResource.setValue(StringUtils.trimWhitespace(token));
			}
			if (ind == 3) {
				applicationResource.setAppVersion((StringUtils.trimWhitespace(token)));
				applicationResource.setApplicationVersion((StringUtils.trimWhitespace(token)));
			}
			ind++;
		}
		
		HTTPResourceValueParser httpParser = new HTTPResourceValueParser();
		httpParser.parse(applicationResource.getValue());
		applicationResource.setServeur(httpParser.getServer());
		applicationResource.setPort(httpParser.getPort());

		return applicationResource;
	}

	private ApplicationResource toApplicationResource(Resource httpResource) {
		
		ApplicationResource applicationResource = new ApplicationResource();
	
		StringTokenizer tokenizer = new StringTokenizer(httpResource.getValue() , "/");
		
		int ind = 0;
		String[] tokenArray = new String[10];
		
		while (tokenizer.hasMoreElements()) {
			
			String token = tokenizer.nextToken();
			tokenArray[ind] = token;
			
			for (Entry<String, String> appEntry : applicationMap.entrySet()) {
				
				if (token.equals(appEntry.getValue())) {
					applicationResource.setApplicationCode(appEntry.getKey());
					applicationResource.setApplicationEnvironnement(org.apache.commons.lang3.StringUtils.removeEnd(tokenArray[1], ".dev.promutuel.local"));
				}
			}
			ind++;
		}
		return applicationResource;
	}

	private Resource extractSMTPResource(String line) {
		
		Resource resource = new Resource();
		resource.setType(ResourceType.SMTP);

		StringTokenizer st = new StringTokenizer(line, "|");
		int ind = 0;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (ind == 2) {
				resource.setValue(StringUtils.trimWhitespace(token));
			}
			ind++;
		}

		SMTPResourceValueParser smtpParser = new SMTPResourceValueParser();
		smtpParser.parse(resource.getValue());
		resource.setServeur(smtpParser.getServer());
		resource.setPort(smtpParser.getPort());

		return resource;
	}

	private Resource extractFTPResource(String line) {
		
		Resource resource = new Resource();
		resource.setType(ResourceType.FTP);

		StringTokenizer st = new StringTokenizer(line, "|");
		int ind = 0;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (ind == 2) {
				resource.setValue(StringUtils.trimWhitespace(token));
			}
			ind++;
		}

		FTPResourceValueParser ftpParser = new FTPResourceValueParser();
		ftpParser.parse(resource.getValue());
		resource.setServeur(ftpParser.getServer());
		resource.setPort(ftpParser.getPort());

		return resource;
	}

	private String extractVersion(String line) {

		String retValue = null;

		StringTokenizer st = new StringTokenizer(line, ":");
		int ind = 0;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (ind == 1) {
				retValue = StringUtils.trimWhitespace(token);
			}
			ind++;
		}

		return retValue;

	}

	private Resource extractDatabaseResource(String line) {

		Resource resource = new Resource();
		resource.setType(ResourceType.DB);

		StringTokenizer st = new StringTokenizer(line, "|");
		int ind = 0;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (ind == 2) {
				resource.setValue(StringUtils.trimWhitespace(token));
			}
			ind++;
		}

		if (resource.getValue().startsWith("jdbc:sqlserver://")) {

			DatabaseSQLServerResourceValueParser sqlServerParser = new DatabaseSQLServerResourceValueParser();
			sqlServerParser.parse(resource.getValue());
			resource.setServeur(sqlServerParser.getServer());
			resource.setPort(sqlServerParser.getPort());
			resource.setDatabaseType(DatabaseType.SQLSERVER.toString());
			resource.setDatabaseName(sqlServerParser.getName());

		} else if (resource.getValue().startsWith("jdbc:db2://")) {

			DatabaseDB2ResourceValueParser db2Parser = new DatabaseDB2ResourceValueParser();
			db2Parser.parse(resource.getValue());
			resource.setServeur(db2Parser.getServer());
			resource.setPort(db2Parser.getPort());
			resource.setDatabaseType(DatabaseType.DB2.toString());
			resource.setDatabaseName(db2Parser.getName());

		}

		return resource;
	}
	
	private Resource extractLDAPResource(String line) {

		Resource resource = new Resource();
		resource.setType(ResourceType.LDAP);

		StringTokenizer st = new StringTokenizer(line, "|");
		int ind = 0;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (ind == 2) {
				resource.setValue(StringUtils.trimWhitespace(token));
			}
			ind++;
		}

		LDAPResourceValueParser ldapParser = new LDAPResourceValueParser();
		ldapParser.parse(resource.getValue());
		resource.setServeur(ldapParser.getServer());
		resource.setPort(ldapParser.getPort());

		return resource;

	}

	
	private Resource extractHTTPResource(String line) {
		
		Resource resource = new Resource();
		resource.setType(ResourceType.HTTP);
		
		StringTokenizer st = new StringTokenizer(line, "|");
		int ind = 0;
		while (st.hasMoreTokens()) {
			String token = st.nextToken();
			if (ind == 2) {
				resource.setValue(StringUtils.trimWhitespace(token));
			}
			ind++;
		}
		
		HTTPResourceValueParser httpParser = new HTTPResourceValueParser();
		httpParser.parse(resource.getValue());
		resource.setServeur(httpParser.getServer());
		resource.setPort(httpParser.getPort());

		return resource;
		
	}

	public EtatSante getEtatSante() {
		return etatSante;
	}

}
