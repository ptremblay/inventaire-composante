package ca.promutuel.csd.inventaire.reader;

import java.util.ArrayList;
import java.util.List;

public class EtatSante {

	private String url = null;
	private String content = null;
	private String version = null;
	private String httpResponseCode = null;
	private String httpResponseMessage = null;
	private boolean requestTimedOut = false;
	private List<Resource> resources = new ArrayList<Resource>();
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<Resource> getResources() {
		return resources;
	}

	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}

	public String getHttpResponseCode() {
		return httpResponseCode;
	}

	public void setHttpResponseCode(String httpResponseCode) {
		this.httpResponseCode = httpResponseCode;
	}

	public String getHttpResponseMessage() {
		return httpResponseMessage;
	}

	public void setHttpResponseMessage(String httpResponseMessage) {
		this.httpResponseMessage = httpResponseMessage;
	}

	public boolean isRequestTimedOut() {
		return requestTimedOut;
	}

	public void setRequestTimedOut(boolean requestTimedOut) {
		this.requestTimedOut = requestTimedOut;
	}
	
}
