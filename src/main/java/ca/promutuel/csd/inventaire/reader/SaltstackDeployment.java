package ca.promutuel.csd.inventaire.reader;

import java.util.ArrayList;
import java.util.List;

public class SaltstackDeployment {

	private String applicationName;
	private List<String> servers = new ArrayList<String>();

	public SaltstackDeployment(String applicationName, List<String> servers) {
		this.applicationName = applicationName;
		this.servers = servers;
	}

	public SaltstackDeployment() {
		// TODO Auto-generated constructor stub
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public List<String> getServers() {
		return servers;
	}

	public void setServers(List<String> servers) {
		this.servers = servers;
	}

}
