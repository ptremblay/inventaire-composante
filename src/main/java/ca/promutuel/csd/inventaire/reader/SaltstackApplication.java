package ca.promutuel.csd.inventaire.reader;

public class SaltstackApplication {

	private String nom;
	private String contexte;
	private String diminutif;
	private String port;

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getContexte() {
		return contexte;
	}

	public void setContexte(String contexte) {
		this.contexte = contexte;
	}

	public String getDiminutif() {
		return diminutif;
	}

	public void setDiminutif(String diminutif) {
		this.diminutif = diminutif;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

}
