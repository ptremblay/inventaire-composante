package ca.promutuel.csd.inventaire.reader.parsers;

import java.util.StringTokenizer;

public class HTTPResourceValueParser implements ResourceValueParser {
	
	private String server;
	private String port;

	@Override
	public void parse(String resourceValue) {
		
		if ((resourceValue.startsWith("http://") || (resourceValue.startsWith("https://")))) {

			StringTokenizer tokenizer = new StringTokenizer(resourceValue,"/");

			int ind = 0;
			int ind2 = 0;
			while (tokenizer.hasMoreTokens()) {
				
				String token = tokenizer.nextToken();
				
				if (ind == 1) {
					
					if (token.contains(":")){
						StringTokenizer tokenizer2 = new StringTokenizer(token,":");
						
						
						while (tokenizer2.hasMoreTokens()) {
							String token2 = tokenizer2.nextToken();
							if (ind2 == 0) {
								server = token2;
							}
							
                            if (ind2 == 1) {
								port = token2;
							}
							ind2++;
						}
						
					} else {
						server = token;
						port = "80";
					}

				}
				ind++;
			}
		}
	}

	public String getServer() {
		return server;
	}

	public String getPort() {
		return port;
	}

}
