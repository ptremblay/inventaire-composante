package ca.promutuel.csd.inventaire.reader.parsers;

import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

public class DatabaseSQLServerResourceValueParser implements
		ResourceValueParser {

	private String server;
	private String port;
	private String databaseName;

	@Override
	public void parse(String resourceValue) {

		if (resourceValue.startsWith("jdbc:sqlserver://")) {

			String prepared = StringUtils.replace(StringUtils.removeStart(resourceValue,"jdbc:sqlserver://"), ";", ":");
			StringTokenizer tokenizer = new StringTokenizer(prepared,":");

			int ind = 0;
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken();
				if (ind == 0) {
					server = token;
				}
				if (ind == 1) {
					port = token;
				}
				if (ind == 2) {
					databaseName = StringUtils.removeStart(token,"databaseName=");
				}
				ind++;
			}
		}
	}

	public String getServer() {
		return server;
	}

	public String getPort() {
		return port;
	}

	public String getName() {
		return databaseName;
	}

}
