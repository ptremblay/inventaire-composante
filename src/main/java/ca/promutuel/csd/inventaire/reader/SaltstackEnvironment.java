package ca.promutuel.csd.inventaire.reader;

import java.util.ArrayList;
import java.util.List;

public class SaltstackEnvironment {
	
	private String envName;
	private List<SaltstackDeployment> deploiements = new ArrayList<SaltstackDeployment>();
	private String equipe;
	private String livraison;
	private String netscaler;
	private String palier;
	private String port_env;
	private String projet;

	public SaltstackEnvironment(String envName,
			List<SaltstackDeployment> deploiements, String equipe,
			String livraison, String netscaler, String palier, String port_env,
			String projet) {
		super();
		this.envName = envName;
		this.deploiements = deploiements;
		this.equipe = equipe;
		this.livraison = livraison;
		this.netscaler = netscaler;
		this.palier = palier;
		this.port_env = port_env;
		this.projet = projet;
	}

	public SaltstackEnvironment() {
		// TODO Auto-generated constructor stub
	}

	public String getEnvName() {
		return envName;
	}

	public void setEnvName(String envName) {
		this.envName = envName;
	}

	
	public List<SaltstackDeployment> getDeploiements() {
		return deploiements;
	}

	public void setDeploiements(List<SaltstackDeployment> deploiements) {
		this.deploiements = deploiements;
	}

	public String getEquipe() {
		return equipe;
	}

	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}

	public String getLivraison() {
		return livraison;
	}

	public void setLivraison(String livraison) {
		this.livraison = livraison;
	}

	public String getNetscaler() {
		return netscaler;
	}

	public void setNetscaler(String netscaler) {
		this.netscaler = netscaler;
	}

	public String getPalier() {
		return palier;
	}

	public void setPalier(String palier) {
		this.palier = palier;
	}

	public String getPort_env() {
		return port_env;
	}

	public void setPort_env(String port_env) {
		this.port_env = port_env;
	}

	public String getProjet() {
		return projet;
	}

	public void setProjet(String projet) {
		this.projet = projet;
	}	

	
}
