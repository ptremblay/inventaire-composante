package tryMe;

import java.util.List;

import ca.promutuel.csd.inventaire.reader.SaltstackEnvironment;
import ca.promutuel.csd.inventaire.reader.SaltstackEnvironmentReader;

public class TrySaltstackEnvironmentReader {

	public static void main(String[] args) {

		List<SaltstackEnvironment> envs = SaltstackEnvironmentReader.newInstance().getEnvironnements();
		
		for (SaltstackEnvironment ssenvs : envs){
			System.out.println(ssenvs);
		}
		
	}

}
