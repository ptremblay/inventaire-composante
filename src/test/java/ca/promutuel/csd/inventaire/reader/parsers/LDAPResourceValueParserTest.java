package ca.promutuel.csd.inventaire.reader.parsers;

import org.junit.Assert;
import org.junit.Test;

public class LDAPResourceValueParserTest {

	@Test
	public void testParse() {
		
		LDAPResourceValueParser parser = new LDAPResourceValueParser();
		parser.parse("ldap://ldapdev.promutuel.local:636");
		
		Assert.assertEquals("ldapdev.promutuel.local", parser.getServer());
		Assert.assertEquals("636", parser.getPort());
	}

}
