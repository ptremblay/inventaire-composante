package ca.promutuel.csd.inventaire.reader.parsers;

import org.junit.Assert;
import org.junit.Test;

public class DatabaseDB2ResourceValueParserTest {

	@Test
	public void testParse() {
		
		DatabaseDB2ResourceValueParser parser = new DatabaseDB2ResourceValueParser();
		parser.parse("jdbc:db2://udbefi.promutuel.local:60050/EDPEF15");
		
		Assert.assertEquals("udbefi.promutuel.local", parser.getServer());
		Assert.assertEquals("60050", parser.getPort());
		Assert.assertEquals("EDPEF15", parser.getName());
		
	}

}
