package ca.promutuel.csd.inventaire.reader.parsers;

import org.junit.Assert;
import org.junit.Test;

public class SMTPResourceValueParserTest {

	@Test
	public void testParse() {
		
		SMTPResourceValueParser smtpParser = new SMTPResourceValueParser();
		smtpParser.parse("smtp://d01fax0001.promutuel.local:25");
		
		Assert.assertEquals("d01fax0001.promutuel.local", smtpParser.getServer());
		Assert.assertEquals("25", smtpParser.getPort());
		
	}

}
