package ca.promutuel.csd.inventaire.reader.parsers;

import org.junit.Assert;
import org.junit.Test;

public class FTPResourceValueParserTest {

	@Test
	public void testParse() {
		
		FTPResourceValueParser ftpParser = new FTPResourceValueParser();
		ftpParser.parse("ftp://eaqogsservirap:21");
		
		Assert.assertEquals("eaqogsservirap", ftpParser.getServer());
		Assert.assertEquals("21", ftpParser.getPort());
		
	}

}
