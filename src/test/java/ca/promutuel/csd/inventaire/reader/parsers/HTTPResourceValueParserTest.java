package ca.promutuel.csd.inventaire.reader.parsers;

import org.junit.Assert;
import org.junit.Test;

public class HTTPResourceValueParserTest {

	@Test
	public void testParse() {
		
		HTTPResourceValueParser httpParser = new HTTPResourceValueParser();
		httpParser.parse("http://efi1-rsi2.dev.promutuel.local/wspolice/etat-sante");

		Assert.assertEquals("efi1-rsi2.dev.promutuel.local", httpParser.getServer());
		Assert.assertEquals("80", httpParser.getPort());
	}
	
	
	@Test
	public void testParseWithPort() {
		
		HTTPResourceValueParser httpParser = new HTTPResourceValueParser();
		httpParser.parse("http://xi50dev.promutuel.local:10004/discount?wsdl");

		Assert.assertEquals("xi50dev.promutuel.local", httpParser.getServer());
		Assert.assertEquals("10004", httpParser.getPort());
	}

}
