package ca.promutuel.csd.inventaire.reader.parsers;

import org.junit.Assert;
import org.junit.Test;

public class DatabaseSQLServerResourceValueParserTest {

	@Test
	public void testParse() {

		DatabaseSQLServerResourceValueParser parser = new DatabaseSQLServerResourceValueParser();
		parser.parse("jdbc:sqlserver://d01sql0008:1433;databaseName=SUP_CAB_EFI_01");
		
		Assert.assertEquals("d01sql0008", parser.getServer());
		Assert.assertEquals("1433", parser.getPort());
		Assert.assertEquals("SUP_CAB_EFI_01", parser.getName());
		
	}

}
