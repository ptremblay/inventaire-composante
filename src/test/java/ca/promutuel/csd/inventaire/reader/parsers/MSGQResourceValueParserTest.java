package ca.promutuel.csd.inventaire.reader.parsers;

import org.junit.Assert;
import org.junit.Test;

public class MSGQResourceValueParserTest {

	@Test
	public void testParse() {	
		
		MSGQResourceValueParser parser = new MSGQResourceValueParser();
		parser.parse("Destination=Audatex Transport, Identifiant=6");
	
		Assert.assertEquals("Audatex Transport", parser.getMsgqDestination());
		Assert.assertEquals("6", parser.getMsgqIdentifiant());
		
	}

}
